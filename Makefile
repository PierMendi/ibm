compile:
	mvn clean install

build-images:
	docker build -t proxy-retoibm:0.1.0 ./nginx
	docker build -t retobase-java:0.1.0 .

run:
	docker-compose up -d
